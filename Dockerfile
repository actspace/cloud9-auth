# 以nodejs 0.12的docker image版本為主
FROM node:0.12
MAINTAINER actfire <actfire@gmail.com>

# 安裝 cloud 9 sdk
RUN git clone git://github.com/c9/core.git /c9sdk
WORKDIR /c9sdk
RUN scripts/install-sdk.sh

# Declare volume for workspace storage
RUN mkdir /workspace
WORKDIR /workspace
VOLUME ["/workspace"]

# WebUI
EXPOSE 80
# Debug Port
EXPOSE 15454
# Node listen port
EXPOSE 3000-3010

# Declare environnement variables
ENV ID=user
ENV PW=password

# Start container services
CMD /usr/local/bin/node /c9sdk/server.js --port 80 -w /workspace --listen 0.0.0.0 --auth $ID:$PW